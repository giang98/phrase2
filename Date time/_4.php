<?php
/**
 * Create a variable $date with period is y = 2020 m = 4 d = 29
 */

date_default_timezone_set("Asia/Ho_Chi_Minh");

$defaultday  = mktime(0,0,0, 4 , 29, 2020);
echo date("d-m-Y", $defaultday);