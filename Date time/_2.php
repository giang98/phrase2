<?php
/**
 * Set timezone 'Asia/Ho_Chi_Minh
 */

date_default_timezone_set("Asia/Ho_Chi_Minh");
$today = date("Y-m-d H:i:s");
echo $today;