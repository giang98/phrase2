<?php
/**
 * Create a script to determine time 29 days ago
 */

date_default_timezone_set("Asia/Ho_Chi_Minh");

$h = date("H");
$m = date("i");
$s = date("s");
$previousdays  = mktime($h, $m, $s, date("m")  , date("d")-29, date("Y"));
echo date("Y-m-d h:i:sa", $previousdays);