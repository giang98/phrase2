<?php
/**
 *  Create a script will count the number of days between current day and birthday
 */

    $target_days = mktime(0,0,0,9,30,2021);
    $today = time();
    $diff_days = (int)($target_days - $today);
    $days = (int)($diff_days/86400);
    print "Days till next birthday: $diff_days days!"."\n";