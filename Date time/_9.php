<?php
/**
 *   Create a function to calculate the factorial of a number (a non-negative integer). The function accepts the number as an argument
 */

$n = 5;
$temp = 1;
$factorial = 0;
$i = 1;

while($i <= $n){
    $temp = $i*$temp;
    $factorial = $factorial + $temp;
    $i++;
}

echo 'Giai thừa của '.$n . ' là : ' .$factorial;