<?php
/*
Create a form to allow a user to reset their password with their username and phone number. If
the username and phone number match entries in the database, you should generate a random
string as a password (make it of reasonable length, alphanumeric), inform the user of the random
string generated, and make appropriate changes in the database. Remember that the database
stores the hashed version of the password.
If the username and phone number combination does not exist, inform the user of the failure to
reset the password, and display the reset form below the message.
*/

        
    $servername = 'localhost';
    $user = 'root';
    $pass = '';
    $dbname = 'crud_pdo';
           
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $phone_number = isset($_POST['phone_number']) ? $_POST['phone_number'] : null;


    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $error = [];

            if(empty($username)) {
                $error['username'] = 'Tên đăng nhập không được để trống!';
            }

            if(empty($phone_number)) {
                $error['phone_number'] = 'Nhập mật khẩu!';
            }

            $sql = "SELECT username, phone_number FROM users 
                    SET password = rad(password);
                    WHERE username = ? AND phone_number = MD5(?) LIMIT 1" ;

            $statement = $conn->prepare($sql);

            $statement->execute([$username, $phone_number]);

            $user = $statement->fetch();

            if($user) {                
                header('Location: create.php');
            } else {
                $error['phone_number'] = 'Tên đăng nhập hoặc mật khẩu không đúng!';
                // echo 'Tên đăng nhập hoặc mật khẩu không đúng!';
            }
            
        }

    } catch(PDOException $e) {
        echo "Connection failed" .$e->getMessage();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
    <div class="container">
        <form class="phuongtrinh" method="POST">
            <h3>Kiểm tra</h3>

            <div class="gr-form">
                <p>Tên đăng nhập</p>
                <input type="text" name="username" value="<?php echo $username ?>">

                <?php if(isset($error['username'])) { ?>
                    <small style="color: red"> <?php echo $error['username'] ?></small>
                <?php } ?>
            </div>

            <div class="gr-form">
                <p>Số điện thoại</p>
                <input type="phone_number" name="phone_number" value="<?php echo $phone_number ?>" >

                <?php if(isset($error['phone_number'])) { ?>
                    <small style="color: red"> <?php echo $error['phone_number'] ?></small>
                <?php } ?>
            </div>

            <button name="next">Đăng nhập</button>

        </form>
        
    </div>
</body>
</html>