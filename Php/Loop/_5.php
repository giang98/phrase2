<?php
/**
 *  5. Bài tập tính S = 1! + 2! + 3! + ... + n! (sử dụng vòng lặp while)
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */


 $n = 5;
 $temp = 1;
 $sum = 0;
 $i = 1;

 while($i <= $n){
     $temp = $i*$temp;
     $sum = $sum + $temp;
     $i++;
 }

 echo 'Tổng là: '.$sum;