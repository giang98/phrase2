<?php
/***
 *  6. Kiểm tra một số có phải là số nguyên tố hay không (sử dụng vòng lặp while)
 *  Input :
 *         + Khai báo number
 *  Output:
 *         + Xác định $number có phải là số nguyên tố hay không ?
 *
 */

function isPrimeNumber($i) {
    $n = 2;
    while ($n < $i) {
        if ($i % $n) {
            $n++;
            continue;
        }
       
        echo $i.' không là số nguyên tố';
        return false;
    }

    echo $i.'  là số nguyên tố';
    return true;
}

isPrimeNumber(9);