<?php
/**
 *  1. Bài tập tính tổng : S = 1 + 2 + 3 + 4 + ... + n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 */

 $n = 10;
 $sum = 0;

 for($i = 0 ; $i <= $n ; $i++) {
     $sum += $i;
 }

 echo 'Tổng = '.$sum;