<?php
/**
 *  2. Bài tập in bảng cửu chương bằng vòng lặp for
 *
 */
?>

<table border="1px">
<tr>
<?php
for($i = 1; $i < 10; $i ++) {
    echo "<td>";
    for($j = 1; $j <= 10; $j ++) {
        echo "$i x $j = " . ($i * $j);
        echo "<br>";
    }
    echo "</td>";
}
?>
</tr>
</table>