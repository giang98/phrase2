<?php
/**
 *  2. Tính tổng : S = 1 + 1*2 + 1*2*3 + 1*2*3*4 + ... + 1*2*3*4*...*n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */

 $n = 3;
 $temp = 1;
 $sum = 0;
 $i = 1;

//  while($i <= $n){
//      $temp = $i*$temp;
//      $sum = $sum + $temp;
//      $i++;
//  }

 for($i = 1; $i < $n; $i++){
     $i = $i * ($i+1); 
     $sum += $i;
 }

 echo 'Tổng là: '.$sum;