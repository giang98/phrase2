<?php
/**
 *  3. Tính tổng số nguyên lẻ tử 1 -> n
 *  Input :
 *         + Khai báo n
 *  Output:
 *         + Kết quả của biểu thức S
 *
 */

 $n = 10;
 $sum = 0;

 for($i = 0; $i <= $n; $i++){
     if($i % 2 == 1){
         $sum += $i;
     }
 }
 
 echo 'Tổng các số lẻ = '.$sum;