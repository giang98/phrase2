<?php
/**
 *  1. Viết chương trình so sánh 2 số
 *  Input :
 *         + firstNumber = 2;
 *         + secondNumber = 9;
 *  Output:
 *         + secondNumber lớn hơn firstNumber
 *
 *  Algorithm :
 *           ...
 *
 */

 $firstNumber = 2;
 $secondNumber = 9;

function compare($firstNumber, $secondNumber) {
    if($firstNumber > $secondNumber) {
        echo 'firstNumber lớn hơn secondNumber';
    }
    elseif ($firstNumber < $secondNumber) {
        echo 'secondNumber lớn hơn firstNumber';
    }
    else echo 'firstNumber bằng secondNumber';
}

compare(2,9);