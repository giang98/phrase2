<?php
/**
 *  3. Tìm kiếm phần tử Vsmart-Live trong mảng
 */

$products = [
    [
        'name' => 'Bphone-2019',
        'price' => 1000000,
    ],
    [
        'name' => 'Vsmart-Live',
        'price' => 3790000,
    ],
    [
        'name' => 'Vsmart-Active',
        'price' => 4890000,
    ],
];


$search = isset($_GET['search']) ? $_GET['search'] : null;

if($search != null) {

    $products = array_filter($products, function($product) use ($search) { // use: sử dụng biến ngoài function.
        return strpos($product['name'], $search) !== false; // strpos: ktra chuỗi trong chuỗi.
    });
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test</title>
</head>
<body>
    <form class="search" method="GET">
        <input type="text" name="search" value="<?php echo $search ?>">
        <button>Search</button>
    </form>

    <table border = '1'>
        <thead>
            <tr>
                <th>STT</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
        </thead>
        
        <tbody>
            <?php 
                foreach ($products as $key => $product) { ?>
                <tr>
                    <td><?php echo ($key + 1) ?></td>
                    <td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                </tr>
            <?php  } ?>
                
        </tbody>
       
    </table>

</body>
</html>

<!-- /**
 * 3.1 Hiển thị phần tử đầu tiên của mảng
 */ -->

 <?php
    $first_element = array_shift($products);

    echo '<br> Phần tử đầu tiên của mảng: <br>';
    echo 'Name : '. $first_element['name'] ."<br>";
    echo 'Price : '. $first_element['price'] ."<br>";
 ?>
