<?php
/**
 *  2. CRUD Array (Thêm sửa xóa phần tử của mảng )
 *
 */

$products = [
    [
        'name' => 'Bphone-2019',
        'price' => 1000000,
    ]
];

$new_product =  [
    'name' => 'Vsmart-Live',
    'price' => 2000000,
];

 $products[] = $new_product;

    session_start();

    $products = $_SESSION['products'] ?? $products;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products</title>
</head>
<body>
    <div class="container">
        <table border = "1" >
            <thead >
                <tr>
                    <th>Index</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($products as $key => $product){ ?>
                    <tr>
                        <td><?php echo ($key+1) ?></td>
                        <td><?php echo $product['name'] ?></td>
                        <td><?php echo $product['price'] ?></td>
                        <td><a href="../array/_2edit.php?key=<?php echo $key; ?>">Sửa</a></td>
                        <td><a href="../array/_2delete.php?key=<?php echo $key; ?>">Xóa</a></td>
                    </tr>                   
                <?php } ?>

                <div class="actions actions--left">
                    <a href="../array/_2create.php">Add New</a>
                </div>

            </tbody>
            
        </table>
    </div>
</body>
</html>