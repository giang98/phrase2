<?php
    $key = $_GET['key'] ?? null;

    if ($key !== null) {
        session_start();
         
        $products = $_SESSION['products'] ?? $products ;

        unset($products[$key]);

        $_SESSION['products'] = $products;

        header('Location: ../Array/_2.php');
    }
    
?>