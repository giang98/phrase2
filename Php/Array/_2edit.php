<?php
    $key = $_GET['key'] ?? null;

    session_start();

    $products = $_SESSION['products'] ?? $product;

    $product = $products[$key];

    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $name = $_POST['name'] ?? null;
        $price = $_POST['price'] ?? null;

        $products[$key] = [
            'name' => $name,
            'price' => $price
        ];

        $_SESSION['products'] = $products;

        header('Location: ../Array/_2.php');
    }

    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/style.css">
    <title>Edit Product</title>
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card__header">
                <h4 class="card__title">Product</h4>
            </div>
            <div class="card__body">
                <form method="post">
                    <div class="form__group">
                        <label for="name" class="form__label">Name</label>
                        <input name="name" type="text" class="form__control" value="<?php echo $product['name'] ?? null; ?>">
                    </div>
                    <div class="form__group">
                        <label for="name" class="form__label">Price</label>
                        <input name="price" type="text" class="form__control" value="<?php echo $product['price'] ?? null; ?>">
                    </div>
                    <div class="form__group">
                        <button>Update</button>
                        <a href="/students/index.php">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>