<?php
/**
 *  1. Tạo profile của bạn (Associative Array)
 */
 
    $myProfile = [
        'name' => 'Tạ Thị Hương Giang',
        'age' => '22',
        'gender' => 'female',
        'hometown' => 'Thái Bình',
        'email' => 'giangtth@kaiyouit.com'
    ];

    foreach($myProfile as $key => $myinfo) {
        echo $key .' : '. $myinfo;
        echo '<br>';
    }