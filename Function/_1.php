<?php
/**
 *   Create a script to get start and end date of a week (by week number) of a particular year
 *   Sample week and year : 10, 2020
 *   Expected Result : Starting date of the week: xx-xx-xx - End date the week: xx-xx-xx
 */

function getStartAndEndDate($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['week_start'] = $dto->format('d-m-Y');
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format('d-m-Y');
    return $ret;
  }
  
  $week_array = getStartAndEndDate(10,2020);
  print_r($week_array);