<?php
/**
 *   Create a function to create a new array of given length using the odd numbers from a given array of positive integers
 * Sample Input:
{1,2,3,5,7,9,10},3
Expected Output:
New array: 1,3,5
 */

function oddArr($arr,$n){
    $newArr = [];
    $length = count($arr);

    for ($i=0; $i< $length; $i++){
        if ($arr[$i] %2 == 1 && count($newArr) < $n){
            $newArr[] = $arr[$i];
        }
    }

    return $newArr;
}

$arr = [1,2,3,5,7,9,10];
print_r(oddArr($arr,3));